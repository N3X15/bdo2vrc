import re, sys, os
from pathlib import Path
from PIL import Image, ImageOps, ImageDraw, ImageChops

# pgw_01_nude_0001_w.dds
REG_FILENAME = re.compile(r'(?P<prefix>pgw)_(?P<lod>\d+)_(?P<name>[a-z]+)(?P<id>(_[0-9]+){1,2})(?P<type>_(n|w|sp|m))?.dds')

def saveAs(im: Image, outfile: Path, description: str) -> None:
    print(f'  -> {outfile} ({description})')
    #print(im2.format, im2.size, im2.mode)
    with outfile.open('wb') as f:
        im.save(f, 'PNG')

def convertWrinkleNormals(outdir: Path, img: Image, infile, basename: str) -> None:
    # alpha -> red
    # B = 1
    R,G,B,A = img.split()
    B = ImageChops.constant(B, 255)
    saveAs(Image.merge('RGB', (A, G, B)), outdir / f'{basename}_Normals.png', 'Normals')

def convertNormals(outdir: Path, img: Image, infile, basename: str) -> None:
    # Unmodified, jsut convert to RGB PNG.
    saveAs(img, outdir / f'{basename}_Normals.png', 'Normals')

def convertColorMap(outdir: Path, img: Image, infile, basename: str) -> None:
    # Unmodified, jsut convert to RGB PNG.
    saveAs(img, outdir / f'{basename}_ID.png', 'ID Map')

def convertAlbedo(outdir: Path, img: Image, infile, basename: str) -> None:
    # Unmodified, jsut convert to RGB PNG.
    saveAs(img, outdir / f'{basename}_Albedo.png', 'Albedo')

def convertHairGradient(outdir: Path, img: Image, infile, basename: str) -> None:
    # Unmodified, jsut convert to RGB PNG.
    saveAs(img, outdir / f'{basename}_Gradient.png', 'Gradient')

def convertSpeculars(outdir: Path, img: Image, infile, basename: str) -> None:
    # BDO uses:
    #   R=Roughness
    #   G=AO
    #   B=Metallic
    #   A=Height
    # Unity 2018:
    #   R=Metallic
    #   G=AO
    #   B=Height
    #   A=Glossiness
    MTL: Image
    RGH: Image
    AO: Image
    GL: Image
    H: Image
    RGH,AO,MTL,H = img.split()
    GL = ImageChops.invert(RGH)
    saveAs(Image.merge('RGBA', (MTL, AO, H, GL)), outdir / f'{basename}_Metallic.png', 'Metallics')

TRANSLATE_TYPE = {
    'w':    ('wrinkle normals',  convertWrinkleNormals),
    'n':    ('standard normals', convertNormals),
    'sp':   ('speculars',        convertSpeculars),
    'm':    ('color map',        convertColorMap),
    'a':    ('albedo',           convertAlbedo),
    'hair': ('hair gradient',    convertHairGradient),
}
def main():
    import argparse

    outdir = Path.cwd() / '_converted'

    argp = argparse.ArgumentParser()
    argp.add_argument('--as', choices=['n', 'w', 'sp', 'm', 'a'], default=None)
    argp.add_argument('--outdir', type=str, default=None, help='Output directory.  Default: (./_converted)')
    argp.add_argument('filename', type=str, nargs='*', help='Files to attempt to convert')
    args = argp.parse_args()

    if args.outdir is not None:
        outdir = Path(args.outdir)

    if not outdir.is_dir():
        outdir.mkdir(parents=True)


    for infile in args.filename:
        basefilename: str = os.path.basename(infile)
        newbasename: str = '_'.join(basefilename.split('_')[:-1])
        #print(basefilename)
        if m := REG_FILENAME.match(basefilename):
            im: Image = Image.open(infile)
            channeltype = ''
            g = {}
            if getattr(args, 'as') is not None:
                channeltype = getattr(args, 'as')
            else:
                g = m.groupdict()
                #print(repr(g))
                if 'type' in g and g['type'] is not None:
                    channeltype = g['type'][1:]
                else:
                    channeltype = 'a'
            converter = None
            description: str = f'Unknown ({channeltype})'
            if channeltype in TRANSLATE_TYPE:
                description, converter = TRANSLATE_TYPE[channeltype]
                _as = getattr(args, 'as')
                if _as is None:
                    description = f'Detected as {description}'
                else:
                    description = f'Forced to be read as {description} (--as={_as})'
            else:
                print(f'>> {infile}: {im.format} ({im.size[0]}x{im.size[1]}')
                print(f'  ! We do not support this image type yet. ({channeltype})')
                print(f'    Please submit an issue with which color channels are which. Thanks!')
                continue

            print(f'>> {infile}: {im.format} ({im.size[0]}x{im.size[1]}) - {description}')

            converter(outdir, im, infile, newbasename)

if __name__ == '__main__':
    main()
